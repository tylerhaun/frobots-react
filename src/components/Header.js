import React from "react";

require("../cssLoader").load(__dirname + "/Header.scss");

const svgIcons = require("../svgIcons");


class Header extends React.Component {
    render() {


        let navButtons = (
            <div className="nav-buttons">
                <div className="nav-button action">
                    <a href="/">
                       Home 
                    </a>
                </div>
                <div className="nav-button action">
                    <a href="/resume">
                        Resume
                    </a>
                </div>
                <div className="nav-button action">
                    <a href="/books">
                        Books
                    </a>
                </div>
            </div>
        );

        let gitlabLink = (
            <div className="source-button action">
                <a href="https://gitlab.com/tylerhaun/frobots-react" target="_blank">
                    <span>View Source</span>
                    {svgIcons.externalLink}
                </a>
            </div>
        );

        if (this.props.mainTitle == "false") {
            var title = (
                <h2 className="title">
                    <a href="/" rel="home">frobots</a>
                </h2>
            );
        }
        else {
            var title = (
                <h1 className="title">
                    <a href="/" rel="home">frobots</a>
                </h1>
            );
        }

        return (
            <header className="Header">
                <div className="main-header">
                    <div className="title-container">
                        {title}
                    </div>
                    {navButtons}
                    {gitlabLink}
                </div>
                <div className="secondary-header">
                    {navButtons}
                </div>
                <script dangerouslySetInnerHTML={{__html: `
                    (function() {

                        var header = document.getElementsByClassName("Header")[0];
                        //var navButtonsContainer = header.getElementsByClassName("nav-buttons")[0];
                        //var navButtons = navButtonsContainer.getElementsByClassName("nav-button");
                        var navButtons = header.getElementsByClassName("nav-button");
                        for (var i=0; i<navButtons.length;i++) {
                            var navButton = navButtons[i];

                            var buttonHref = navButton.children[0].href;
                            var windowHref = window.location.href;

                            if (buttonHref == windowHref) {
                                navButton.classList.add("current");
                            }
                        }
                    })()
                `}}>
                </script>
            </header>
        );
    }
}

export default Header;
