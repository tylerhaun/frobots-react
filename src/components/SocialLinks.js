import React from "react";

require("../cssLoader").load(__dirname + "/SocialLinks.scss");

const icons = require("../svgIcons");

import pages from "./pages";

const socialLinksData = [{
        id: "facebook",
        name: "Facebook",
        icon: icons.facebook,
        url: "https://www.facebook.com/tyler.haun"
    }, {
        id: "twitter",
        name: "Twitter",
        icon: icons.twitter,
        url: "https://twitter.com/frodo0321",
        display: false
    }, {
        id: "github",
        name: "GitHub",
        icon: icons.github,
        url: "https://github.com/frodo0321"
    }, {
        id: "gitlab",
        name: "GitLab",
        icon: icons.gitlab,
        url: "https://gitlab.com/tylerhaun"
    }, {
        id: "angellist",
        name: "AngelList",
        icon: icons.angellist,
        url: "https://angel.co/tyler-haun"
    }, {
        id: "linkedin",
        name: "LinkedIn",
        icon: icons.linkedin,
        url: "https://www.linkedin.com/in/tylerhaun/"
    }
];

class SocialLinks extends React.Component {
    render() {

        return (
            <div className="SocialLinks">
                {socialLinksData.map(link => {
                    return (
                        <a key={link.id} target="_blank" className="social-link" href={link.url}>
                            <span className="icon">{link.icon}</span>
                            <span className="name">{link.name}</span>
                            <span className="icon external-link">{icons.externalLink}</span>
                        </a>
                    );
                })}
            </div>
       );
    }
}

export default SocialLinks;
