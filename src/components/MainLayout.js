import React from "react";

import Header from "./Header";
import LeftSidebar from "./LeftSidebar";
import Footer from "./Footer";

require("../cssLoader").load(__dirname + "/MainLayout.scss");

const config = require("../config");

class MainLayout extends React.Component {

    render() {

        let googleAdSenseCode = (
            <div className="ad-container" dangerouslySetInnerHTML={{__html: `
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Test -->
                <ins class="adsbygoogle"
                     style="display:block"
                    data-ad-client="ca-pub-9511215937776215"
                       data-ad-slot="4152199945"
                        data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            `}}>
            </div>
        );


        return (
            <div className="MainLayout">
                <Header mainTitle={this.props.mainTitle} />
                <div className="main">
                    <div className="left-sidebar-container">
                        <LeftSidebar />
                    </div>
                    <div className="main-content">
                        {this.props.children}
                    </div>
                    <div className="right-sidebar-placeholder">
                        {config.activateGoogleAdSense == true ? googleAdSenseCode : ""}
                    </div>
                </div>
                <Footer />
            </div>
       );
    }
}

export default MainLayout;
