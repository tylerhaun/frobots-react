import React from "react";
import moment from "moment";

export const title = "How To Block IP Addresses from Accessing a Server";
export const date = moment("2018-06-08T20:17:27-07:00");
export const published = true;
module.exports.public = true;

export const preview = (
    <div>
        Nowadays there are tons of bots out there, some good some bad.  But how to do block the bad ones from accessing your server?
    </div>
);



class Post extends React.Component {

    constructor(props) {
        super(props);
    
    }

    render() {

        return (
            <div className="post-content">
                {preview}

                <p>
                    For me, there were a bunch of really annoying requests coming in from some presumably bot servers trying to hack me?  Not exactly sure what their purpose was, but they were muddying up my logs so I wanted them gone.
                </p>

                <img src="/images/Screenshot_from_2018-06-08_19-39-54.png"></img>

                <a href="https://wiki.ubuntu.com/UncomplicatedFirewall">UFW (Uncomplicated Firewall)</a> is a really simple and useful firewall tool on linux.  With it you're able to defined rules to either allow or block ip addresses, ports, etc.

                It is by far the easiest way I know of to block ip addresses from a server.

                The command to block an ip address is simply

                <pre className="code">
                    {`$ sudo ufw deny from [ip] to any`}
                </pre>

                In this case:
                <pre className="code">
                    {`$ sudo ufw deny from 167.114.0.63 to any
Rule added`}
                </pre>

                <p>
                    except when I checked the logs I still saw the annoying requests still coming in.
                </p>

                With UFW rules have an order to them and the deny rules I added went after other rules which rendered them useless.  

                <pre className="code">
                    {`$ sudo ufw status numbered
Status: active

     To                         Action      From
          --                         ------      ----
          [ 1] 22                         ALLOW IN    Anywhere
          [ 2] 80                         ALLOW IN    Anywhere
          [ 3] 443                        ALLOW IN    Anywhere
          [ 4] 115/tcp                    ALLOW IN    Anywhere
          [ 5] Anywhere                   DENY IN     37.187.139.66
          [ 6] Anywhere                   DENY IN     167.114.0.63
          [ 7] 22 (v6)                    ALLOW IN    Anywhere (v6)
          [ 8] 80 (v6)                    ALLOW IN    Anywhere (v6)
          [ 9] 443 (v6)                   ALLOW IN    Anywhere (v6)
          [10] 115/tcp (v6)               ALLOW IN    Anywhere (v6)`}
                </pre>
                
                And to delete a rule:
                <pre className="code">
                    {`$ sudo ufw delete 5
Deleting:
 deny from 37.187.139.66
Proceed with operation (y|n)? y   
Rule deleted`}
                </pre>

                Then to add a rule at the top so it overrides all other rules:

                <pre className="code">
                    {`$ sudo ufw insert 1 deny from 167.114.0.63 to any`}
                </pre>
                
                <p>
                    And with that, the bot requests are no longer getting in.
                </p>

            </div>
       );
    }
}

export const component = Post;
export default Post;
