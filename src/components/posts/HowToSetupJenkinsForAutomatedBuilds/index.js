import React from "react";
import moment from "moment";

export const title = "How to Set Up Jenkins for Automated Builds";
export const date = moment("2018-09-12T00:17:15.484Z");
export const published = true;
module.exports.public = true;


export const preview = (
    <div>
        <p>
            Jenkins is a powerful tool to help automate software development.
        </p>
    </div>
);


class Post extends React.Component {

    constructor(props) {
        super(props);
    
    }

    render() {

        return (
            <div className="post-content">

                {preview}

                <p>
                    In a previous company I worked for, one of the projects I worked on was setting up jeknins on out staging and production servers to speed up the build time and bring us closer to being able to do multiple releases a day instead of one every few weeks.
                </p>

                <p>
                    Normally we would ssh into the server and build it by hand which would sometimes take up to 30 minutes for some larger projects with a lot of dependencies.  
                </p>

                <p>
                    But after we got Jenkins set up, it was as easy as clicking a button.
                </p>

                <p>
                    We were using the MEAN stack with pm2, so this tutorial will be based around that.
                </p>

                <a href="https://wiki.jenkins.io/display/JENKINS/Installing+Jenkins+on+Ubuntu" target="_blank">From the jenkins website:</a>
                <pre className="code">{`wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins`}
                </pre>

                <p>
                    Then after it gets installed, it should be automatically started on port 8080.
                </p>

                <p>
                    After going to the dashboard, you should be contronted with a screen that asks you for an administrator password which is at the given location.
                </p>
                <img src="/images/Screenshot_from_2018-09-11_14-16-49.png"></img>

                To get it, just type
                <pre className="code">{`$ sudo cat /var/lib/jenkins/secrets/initialAdminPassword
3de96e6ce06a4dff0460653036ea6a16                
                `}</pre>

                <p>
                    Then enter in the displayed password into the dashboard to get access.
                </p>

                <p>
                    It will show a screen asking about plugins, I usually just install the recommended plugins to make things easier.
                </p>
                <img src="/images/Screenshot_from_2018-09-11_14-23-33.png"></img>

                <p>
                    Fill out the form with your info
                </p>
                <img src="/images/Screenshot_from_2018-09-11_14-24-31.png"></img>

                <p>
                    Then when it asks about the url, I don't think it matters at all for the scope of this project so you can either save or skip.
                </p>

                <p>
                    Then create a new job.  I use the <i>Freestyle project</i> configuration
                </p>
                <img src="/images/Screenshot_from_2018-09-11_14-35-33.png"></img>

                <p>
                Then go to the project screen, click configure.
                </p>
                <img src="/images/Screenshot_from_2018-09-11_14-38-59.png"></img>

                <p>
                Optionally you can add in parameters which you can fill out when you build it.  I add branch and tag parameters to give more flexibility.
                </p>
                <img src="/images/Screenshot_from_2018-09-11_14-44-59.png"></img>

                <p>
                In the Source Code Management tab, add a git SCM and enter in the git repo.  If it is private you will have to add a credentials to it, which in my experience SSH keys are the easiest.
                And also in Aditional Behaviors, add <span className="inline-code">Wipe out repository & force clone</span>.  This makes sure that no old files get left behind and potentially break things.
                </p>
                <img src="/images/Screenshot_from_2018-09-11_16-34-39.png"></img>

                <p>
                Then we can ignore build triggers for now.  You can do a lot of cool things with it, like using a webhook to trigger a build, like when someone pushes a commit.  But with no triggers set, they have a button on the project page which allows you to trigger it manually, which is the desired action here.
                </p>

                <p>
                And what I also do for ease and security is create a separate user to hold the pm2 process.  Normally all of the pm2 process info goes into $HOME/.pm2 and when it gets run with the jenkins user it gets a little weird.
                </p>
                So to create the pm2 user I run these commands:
                <pre className="code">{`sudo adduser pm2-user
sudo adduser jenkins pm2-user`}</pre>

                Restart jenkins so the new group takes effect

                <pre className="code">{`sudo service jenkins restart`}</pre>

                And to login as the user:

                <pre className="code">{`sudo -u pm2-user -s`}</pre>

                And now to update the permissions so the jenkins user can access everything.

                <pre className="code">{`export HOME=/home/pm2-user
cd ~
chmod g+rwx .
chmod -R g+rwx .pm2`}
                </pre>

                <p>
                    Now back in Jenkins, install the <a href="https://wiki.jenkins.io/display/JENKINS/NodeJS+Plugin">NodeJS plugin</a> which just allows jenkins to use npm and node.  Add in a pm2 dependency and that should be good to go.
                </p>
                <img src="/images/Screenshot_from_2018-09-11_17-07-18.png"></img>

                <p>
                    And then edit the config to add in NodeJS and the build steps like so.
                </p>

                <pre className="code">{`git checkout $Branch
npm install
export HOME=/home/pm2-user
npm run restart:pm2`}
                </pre>
                <img src="/images/Screenshot_from_2018-09-11_16-45-09.png"></img>

                
                <p>
                    And now it should be good to go.  Test the build by going to the job page, click <span className="inline-code">Build With Parameters</span>, then <span className="inline-code">Build</span>
                </p>
                <img src="/images/Screenshot_from_2018-09-11_16-50-33.png"></img>

                <p>
                    If everything is done right, the build should succeed and the pm2 process should be running.
                </p>
                <img src="/images/Screenshot_from_2018-09-11_16-56-16.png"></img>

                <div style={{paddingBottom: "10em"}}></div>
            </div>
       );
    }
}

export const component = Post;
export default Post;

