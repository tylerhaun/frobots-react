import handlebars from "handlebars";
import path from "path";
import fs from "fs";
import config from "../config"

var pathToTemplate = path.resolve(__dirname, "../index.hbs");

var template;

fs.readFile(pathToTemplate, function(error, data) {
    if (error) {
        console.error(error);
    }
    data = data.toString();
    template = handlebars.compile(data.toString());
})



const Html = function Html(args) {

    return template(Object.assign({}, config, args));

}

export default Html;
