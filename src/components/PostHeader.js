import React from "react";
import moment from "moment";

require("../cssLoader").load(__dirname + "/PostHeader.scss");

import {postToUrl} from "../utils";

import {clock} from "../svgIcons";

class PostHeader extends React.Component {

    constructor(props) {
        super(props);
    
    }

    render() {

        let post = this.props.post;

        let postDate = moment(post.date);
        let displayDateString = postDate.format("MMM DD, YYYY");

        let postUrl = postToUrl(post);

        if (this.props.mainTitle == "true") {
            var title = (
                <h1 className="post-title">
                    <a href={postUrl}>{post.title}</a>
                </h1>
            );
        }
        else {
            var title = (
                <h2 className="post-title">
                    <a href={postUrl}>{post.title}</a>
                </h2>
            );
        }

        return (
            <div className="PostHeader">
                {title}
                <div className="post-meta">
                    <span className="post-date">
                        <a href={postUrl}>
                            <span>
                                {clock}
                                {displayDateString}
                            </span>
                        </a>
                    </span>
                    <span className="post-author">
                        <div className="post-author-image">
                            <img src="/images/tylerhaun.jpg"></img>
                        </div>
                        <span className="post-author-name">Tyler Haun</span>
                    </span>
                </div>
            </div>
       );
    }
}

export default PostHeader;
