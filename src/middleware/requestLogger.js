import moment from "moment-timezone";
const onHeaders = require("on-headers");
const onFinished = require("on-finished");


function getFormatLogData(request, response) {

    var ms = (response._hrtime[0] - request._hrtime[0]) * 1e3 +
    (response._hrtime[1] - request._hrtime[1]) * 1e-6

    // return truncated value
    const elapsedTime = ms.toFixed(3)

    const ip = request.headers["x-real-ip"] || request.ip;

    const logData = {
        timestampH: moment().tz("America/Los_Angeles").format("MMMM Do, YYYY hh:mm:ss.SSS A Z"),
        request: {
            method: request.method,
            path: request.path,
            ip,
            headers: request.headers,
            query: request.query,
            body: request.body,
        },
        timestamp: moment().format(),
        sessionId: request.sessionId,
        response: {
            headers: response._headers,
            elapsedTime: elapsedTime,
            statusCode: response.statusCode
        }
    }
    if (response.locals.error) {
        logData.response.error = response.locals.error;
    }

    return logData;

}


module.exports = function(app) {

    app.use(function requestLogger(request, response, next) {

        // record hrtime for request
        recordHrtime.call(request);


        function recordHrtime () {
            // this is a reference to request or response
            this._hrtime = process.hrtime();
        }

        // record hrtime for response
        onHeaders(response, recordHrtime);


        function sendLog() {
            const formattedData = JSON.stringify(getFormatLogData(request, response));
            console.log(formattedData);
        }

        onFinished(response, sendLog);

        return next();

    })

}
