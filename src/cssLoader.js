
function CssLoader() {

    const nodeSass = require("node-sass");
    const path = require("path");
    const fs = require("fs");

    this.dirty = false;

    this.cssList = [];
    this.scssList = [];

    this.load = function load(filePath, opts) {
        opts = opts || {};
        let fileData = fs.readFileSync(filePath)
        if (path.extname(filePath) == ".css") {
            this.cssList.push({
                order: opts.order,
                path: filePath,
                data: fileData
            });
        }
        if (path.extname(filePath) == ".scss") {
            this.scssList.push({
                order: opts.order,
                path: filePath,
                data: fileData
            });
        }
        this.dirty = true;
    }
    this.compileScss = function compileScss() {

        let css = "";
        if (this.scssList.length > 0) {
            this.scssList.sort(function sort(a, b) {
                    let aOrder = isNaN(a.order) ? Infinity : a.order;
                    let bOrder = isNaN(b.order) ? Infinity : b.order;
                    let compareValue = aOrder - bOrder;
                    return compareValue;
                });

            let scss = this.scssList.reduce((result, e) => result + e.data, "")

            let result = nodeSass.renderSync({
                    data: scss,
                    outputStyle: "expanded"
                });
            css += result.css.toString();
        }
        return css;
    
    }
    this.compileCss = function compileCss() {
        let css = "";
        if (this.cssList.length > 0) {
        
            this.cssList.sort(function sort(a, b) {
                    let aOrder = isNaN(a.order) ? Infinity : a.order;
                    let bOrder = isNaN(b.order) ? Infinity : b.order;
                    let compareValue = aOrder - bOrder;
                    return compareValue;
                });

            css = this.cssList.reduce((result, e) => result + e.data, "")
        }
        return css;
    }
    this.compile = function compile() {
        if (this.dirty == false) {
            return this.compiledCss;
        }
        let css = "";

        css += this.compileScss();
        css += this.compileCss();

        this.compiledCss = css;
        this.dirty = false;
        return this.compiledCss;
    }
}

module.exports = new CssLoader();
