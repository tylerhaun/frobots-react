
let configPath = "/etc/frobots.json";

let config = {};
try {
    config = require(configPath);
}
catch (e) {
    console.warn("Enable to read config file at", configPath, e);
}

module.exports = config;
